import os
from pydash import camel_case
import json

def extract_cell(lines):
    arr = []
    cell = ""
    for line in lines:
        line = line.strip()
        if line == '': continue
        if "# In[" not in line:
            cell+=line+"\n"
        if "# In[" in line:
            cell.strip()
            arr.append(cell)
            cell = ""
    return arr


# text = open("/Users/vaisakh/programs/NLP/Source/Movie_reviews.py", "r").readlines()
# cells = extract_cell(text)
# print(cells[4])


ROOT = "/Users/vaisakh/programs/Notebooks/Source"

bundle = {}
files = os.listdir(ROOT)
for file in files:
    file_name = camel_case(file.split(".")[0])
    path = os.path.join(ROOT, file)
    text = open(path, "r").readlines()
    bundle[file_name] = extract_cell(text)

bundle["msg"] = "null"

print(bundle.keys())
json.dump(bundle, open("bundle.json", "w"), indent=4)

