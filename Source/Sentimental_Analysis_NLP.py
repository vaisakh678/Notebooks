#!/usr/bin/env python
# coding: utf-8

# In[ ]:


4import tensorflow as tf
import pandas as pd
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, LSTM, SimpleRNN
import matplotlib.pyplot as plt


# In[ ]:


df = pd.read_csv("/content/drive/MyDrive/Datasets/yelp_labelled.txt",names = ['Review', 'label'], sep = '\t')


# In[ ]:


df.head()


# In[ ]:


from sklearn.model_selection import train_test_split
reviews = df['Review'].values
y = df['label'].values


# In[ ]:


review_train,review_test,y_train,y_test = train_test_split(reviews,y,test_size = 0.25,random_state =1000)


# In[ ]:


review_train.shape


# In[ ]:


from sklearn.feature_extraction.text import CountVectorizer

vectorizer = CountVectorizer()
vectorizer.fit(review_train)

X_train = vectorizer.transform(review_train)
X_test = vectorizer.transform(review_test)

X_train


# In[ ]:


X_test


# In[ ]:


X_train.shape


# In[ ]:


from keras.models import Sequential
from keras.layers import Dense

input_dim = X_train.shape[1]

model = Sequential()

model.add(Dense(20,input_dim = input_dim,activation = 'tanh'))
model.add(Dense(1,activation = 'sigmoid'))
model.compile(loss = 'binary_crossentropy', optimizer = 'adam', metrics = ['accuracy'])
model.summary()


# In[ ]:


history = model.fit(X_train,y_train, epochs = 10,validation_data = (X_test,y_test),batch_size = 10)


# In[ ]:


lss , acc = model.evaluate(X_test,y_test)
print(acc)


# Vectorization Another Approach

# In[ ]:


from keras.preprocessing.text import Tokenizer
tokenizer = Tokenizer(num_words = 5000)

tokenizer.fit_on_texts(review_train)

X_train = tokenizer.texts_to_sequences(review_train)
X_test = tokenizer.texts_to_sequences(review_test)

#Adding 1 bcoz of reserved 0 index
vocab_size = len(tokenizer.word_index)+1

print(review_train[2])
print(X_train[2])


# In[ ]:


len(tokenizer.word_index)+1


# In[ ]:


from  keras_preprocessing.sequence import pad_sequences
maxlen = 100

X_train = pad_sequences(X_train, padding = 'post',maxlen = maxlen)
X_test = pad_sequences(X_test, padding = 'post',maxlen = maxlen)


# In[ ]:


#Model 2
from keras.models import Sequential
from keras.layers import Dense,Embedding,Flatten

embedding_dim = 50

model2 = Sequential()

model2.add(Embedding(input_dim=vocab_size,output_dim = embedding_dim, input_length = maxlen))
model2.add(Flatten())
model2.add(Dense(10,activation = 'relu'))
model2.add(Dense(1,activation = 'sigmoid'))

model2.compile(loss = 'binary_crossentropy', optimizer = 'adam', metrics = ['accuracy'])
model2.summary()


# In[ ]:


history = model2.fit(X_train,y_train, epochs = 10,validation_data = (X_test,y_test),batch_size = 10)


# # CNN

# In[ ]:


#Model 3
embedding_dim = 300

from keras.models import Sequential
from keras.layers import Dense,Embedding,Conv1D,GlobalMaxPool1D
model3 = Sequential()
model3.add(Embedding(input_dim = vocab_size,output_dim = embedding_dim, input_length = maxlen))
model3.add(Conv1D(32,5, activation = 'relu'))
model3.add(GlobalMaxPool1D())
model3.add(Dense(1,activation = 'sigmoid'))

model3.compile(loss = 'binary_crossentropy', optimizer = 'adam', metrics = ['accuracy'])
model3.summary()


# In[ ]:


history = model3.fit(X_train,y_train, epochs = 20,validation_data = (X_test,y_test),batch_size = 10)


# # RNN model

# In[ ]:


#Model 4
from keras.models import Sequential
from keras.layers import Dense,Embedding,SimpleRNN,Dropout
model4 = Sequential()
model4.add(Embedding(input_dim = vocab_size,output_dim = embedding_dim, input_length = maxlen))
model4.add(SimpleRNN(128, return_sequences = True))
model4.add(Dropout(0.2))
model4.add(SimpleRNN(128))
model4.add(Dropout(0.2))
model4.add(Dense(10,activation = 'relu'))
model4.add(Dense(1,activation = 'sigmoid'))

model4.compile(loss = 'binary_crossentropy', optimizer = 'adam', metrics = ['accuracy'])
model4.summary()


# In[ ]:


history = model4.fit(X_train,y_train, epochs = 5,validation_data = (X_test,y_test),batch_size = 10)


# # LSTM

# In[ ]:


#Model 5
from keras.models import Sequential
from keras.layers import Dense,Embedding,LSTM,Dropout
model5 = Sequential()
model5.add(Embedding(input_dim = vocab_size,output_dim = embedding_dim, input_length = maxlen))
model5.add(LSTM(128, return_sequences = True))
model5.add(Dropout(0.2))
model5.add(LSTM(128))
model5.add(Dropout(0.2))
model5.add(Dense(10,activation = 'relu'))
model5.add(Dense(1,activation = 'sigmoid'))

model5.compile(loss = 'binary_crossentropy', optimizer = 'adam', metrics = ['accuracy'])
model5.summary()


# In[ ]:


history = model5.fit(X_train,y_train, epochs = 20,validation_data = (X_test,y_test),batch_size = 10)

