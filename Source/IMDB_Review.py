#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import tensorflow as tf
import pandas as pd
from keras.preprocessing.text import Tokenizer
from keras_preprocessing.sequence import pad_sequences
from sklearn.preprocessing import LabelBinarizer
import keras
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt


# In[ ]:


df = pd.read_csv("/content/drive/MyDrive/Datasets/IMDB Dataset.csv")


# In[ ]:


df.head()


# In[ ]:


import nltk
nltk.download('stopwords')
nltk.download('punkt')
import re


# In[ ]:


from nltk.corpus import stopwords
s = stopwords.words("english")

df['clean_review']  = df['review'].str.lower()


# In[ ]:


df.head()


# In[ ]:


df['clean_review'] = df['clean_review'].apply(lambda x: " ".join(i for i in x.split() if i not in s))


# In[ ]:


df.head()


# In[ ]:


df['clean_review'] = df['clean_review'].apply(lambda x: "".join(re.findall(r'[a-zA-Z+" "]',x)))
df['clean_review'] = df['clean_review'].apply(lambda x: "".join(re.sub(r'[<*>]','',x)))


# In[ ]:


df.head()


# In[ ]:


from sklearn.preprocessing import LabelEncoder
le = LabelEncoder()
df['sentiment_label'] = le.fit_transform(df['sentiment'])


# In[ ]:


df.head()


# In[ ]:


#X = df.drop(columns= ["review","sentiment","sentiment_label"],axis = 1)
X = df['clean_review']
Y = df['sentiment_label']


# In[ ]:


tokenizer = Tokenizer(num_words = 5000)
tokenizer.fit_on_texts(X)

words_to_index = tokenizer.word_index


# In[ ]:


words_to_index


# In[ ]:


X_train,X_test,y_train,y_test = train_test_split(X,Y,test_size = 0.2,random_state = 0)


# In[ ]:


import numpy as np
def read_glove_vector(glove_vec):
  with open(glove_vec,'r',encoding = 'UTF-8') as f:
    words = set()
    word_to_vec_map = {}
    for line in f:
      w_line = line.split()
      curr_word = w_line[0]
      word_to_vec_map[curr_word] = np.array(w_line[1:],dtype = np.float64)
  return word_to_vec_map

word_to_vec_map = read_glove_vector('/content/drive/MyDrive/Datasets/glove.6B.50d.txt')


# In[ ]:


maxLen = 150


# In[ ]:


vocab_len = len(words_to_index)


# In[ ]:


embed_vector_len = word_to_vec_map['killing'].shape[0]


# In[ ]:


emb_matrix = np.zeros((vocab_len, embed_vector_len))
for word, index in words_to_index.items():
  embedding_vector = word_to_vec_map.get(word)
  if embedding_vector is not None:
    emb_matrix[index, :] = embedding_vector


# In[ ]:


X_train_indices = tokenizer.texts_to_sequences(X_train)
X_train_indices = pad_sequences(X_train_indices, maxlen=maxLen, padding='post')
X_test_indices = tokenizer.texts_to_sequences(X_test)
X_test_indices = pad_sequences(X_test_indices, maxlen=maxLen, padding='post')


# In[ ]:


from keras.models import Sequential
from keras.layers import Dense,Embedding,LSTM,Dropout
model = Sequential()
model.add(Embedding(input_dim = vocab_len,output_dim = embed_vector_len, input_length = maxLen,weights = [emb_matrix], trainable=False))
model.add(LSTM(128, return_sequences = True))
model.add(Dropout(0.2))
model.add(LSTM(128, return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(128, return_sequences=True))
model.add(Dense(1,activation = 'sigmoid'))

model.compile(loss = 'binary_crossentropy', optimizer = 'adam', metrics = ['accuracy'])
model.summary()


# **Alert: Colab may crash before completing 13th epoch**

# Time Taken for reaching 13th epoch is 1 Hr 44 Mins

# **After 15 epoch: loss: 0.2625 - accuracy: 0.8843 - val_loss: 0.4476 - val_accuracy: 0.8045**

# In[ ]:


history = model.fit(X_train_indices, y_train,epochs=15,validation_data=(X_test_indices, y_test),batch_size=64)


# In[ ]:




