#!/usr/bin/env python
# coding: utf-8

# ###**IMDB Dataset Sentiment analysis**

# In[ ]:


import tensorflow as tf
import keras
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelBinarizer
from sklearn.preprocessing import LabelEncoder
from keras.preprocessing.text import Tokenizer
from keras_preprocessing.sequence import pad_sequences


# In[ ]:


data = pd.read_csv('/content/drive/MyDrive/IMDB Dataset.csv')
data.head()


# In[ ]:


import nltk
nltk.download('stopwords')
nltk.download('punkt')
from nltk.corpus import stopwords
s = stopwords.words('english')


# In[ ]:


data['clean_review'] = data['review'].str.lower()
data.head()


# In[ ]:


import re
data['clean_review'] = data['clean_review'].apply(lambda x:''.join(re.compile(r'<.*?>').sub('',x)))
data.head()


# In[ ]:


data['clean_review'] = data['clean_review'].apply(lambda x:' '.join(i for i in x.split() if i not in s))
data.head()


# In[ ]:


data['clean_review'] = data['clean_review'].apply(lambda x: ''.join(re.findall(r'[a-zA-Z" "]',x)))
data.head()


# In[ ]:


# data['clean_review'] = data['review'].str.lower()
# data['clean_review'] = data['clean_review'].apply(lambda x:''.join(re.compile(r'<.*?>').sub('',x)))
# data['clean_review'] = data['clean_review'].apply(lambda x:' '.join(i for i in x.split() if i not in s))
# data['clean_review'] = data['clean_review'].apply(lambda x:''.join(re.findall(r'[a-zA-Z" "]',x)))
# data.head()


# In[ ]:


le=LabelEncoder()
data['new_sentiment'] = le.fit_transform(data['sentiment'])
data.head()


# In[ ]:


X = data['clean_review']
Y = data['new_sentiment']


# In[ ]:


tokenize = Tokenizer(num_words = 5000)
tokenize.fit_on_texts(X)
word_index =  tokenize.word_index
word_index


# In[ ]:


len(word_index)


# In[ ]:


x_train,x_test,y_train,y_test = train_test_split(X,Y,test_size=0.2,random_state=0)


# In[ ]:


def read_glove_vector(glove_file):
  with open(glove_file,'r',encoding='UTF-8') as f:
    word_to_vec_map={}
    for line in f:
      words=line.split()
      curr_word = words[0]
      word_to_vec_map[curr_word] = np.array(words[1:],dtype=np.float64)
    return word_to_vec_map

word_to_vec_map = read_glove_vector('/content/drive/MyDrive/glove.6B.50d.txt')      


# In[ ]:


maxLen=150
vocab_len = len(word_index)
embed_vec_len = word_to_vec_map['it'].shape[0]


# In[ ]:


emb_matrix = np.zeros((vocab_len,embed_vec_len))
for word,index in word_index.items():
  embedding_vector = word_to_vec_map.get(word)
  if embedding_vector is not None:
    emb_matrix[index,:] = embedding_vector


# In[ ]:


x_train_indices = tokenize.texts_to_sequences(x_train)
x_train_indices = pad_sequences(x_train_indices,maxlen=maxLen,padding='post')
x_test_indices = tokenize.texts_to_sequences(x_test)
x_test_indices = pad_sequences(x_test_indices,maxlen=maxLen,padding='post')


# In[ ]:


from keras.models import Sequential
from keras.layers import Dense,Dropout,Embedding,LSTM
model = Sequential()
model.add(Embedding(input_dim=vocab_len,output_dim=embed_vec_len,input_length=maxLen,weights=[emb_matrix],trainable=False))
model.add(LSTM(128,return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(128,return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(128,return_sequences=True))
model.add(Dense(1,activation='sigmoid'))

model.compile(loss='binary_crossentropy',optimizer='adam',metrics=['accuracy'])
model.summary()


# In[ ]:


history = model.fit(x_train_indices,y_train,epochs=15,validation_data=(x_test_indices,y_test),batch_size=64)


# ###**Yelp Dataset Sentiment analysis**

# In[ ]:


import tensorflow as tf
import keras
from keras.models import Sequential
from keras.layers import Dense
from sklearn.model_selection import train_test_split
import pandas as pd


# In[ ]:


# data = pd.read_csv('/content/drive/MyDrive/yelp_labelled.txt',names=['Review','Label'],sep='\t')
data = pd.read_csv('/content/yelp_labelled.txt',names=['Review','Label'],sep='\t')
data.head()


# In[ ]:


X = data['Review'].values
Y = data['Label'].values


# In[ ]:


review_train,review_test,y_train,y_test = train_test_split(X,Y,test_size=0.25,random_state=1000)


# In[ ]:


from sklearn.feature_extraction.text import CountVectorizer
cv = CountVectorizer()
cv.fit(review_train)

x_train = cv.transform(review_train)
x_test = cv.transform(review_test)                      


# In[ ]:


x_train.shape


# In[ ]:


#Model1-ANN

input_dim = x_train.shape[1]
model1 = Sequential()
model1.add(Dense(20,input_dim=input_dim,activation='leaky_relu'))
model1.add(Dense(1,activation='sigmoid'))
model1.compile(loss='binary_crossentropy',optimizer='adam',metrics=['accuracy'])
model1.summary()


# In[ ]:


history = model1.fit(x_train,y_train,epochs=15,validation_data=(x_test,y_test),batch_size=20)


# In[ ]:


loss,accuracy = model1.evaluate(x_test,y_test,verbose=False)
print("Testing accuracy : {:.4f}".format(accuracy))


# In[ ]:


#Model2 - Embedding layer

from keras.preprocessing.text import Tokenizer
from keras_preprocessing.sequence import pad_sequences

tokenizer = Tokenizer(num_words=5000)
tokenizer.fit_on_texts(review_train)

maxlen=100
x_train = tokenizer.texts_to_sequences(review_train)
x_train = pad_sequences(x_train,maxlen=maxlen,padding='post')
x_test = tokenizer.texts_to_sequences(review_test)
x_test = pad_sequences(x_test,maxlen=maxlen,padding='post')

vocab_size = len(tokenizer.word_index)
embedding_dim_size=50


# In[ ]:


from keras.layers import Embedding,Flatten
model2 = Sequential()
model2.add(Embedding(input_dim=vocab_size,output_dim=embedding_dim_size,input_length=maxlen))
model2.add(Flatten())
model2.add(Dense(10,activation='leaky_relu'))
model2.add(Dense(1,activation='sigmoid'))
model2.compile(loss='binary_crossentropy',optimizer='adam',metrics=['accuracy'])
model2.summary()


# In[ ]:


history = model2.fit(x_train,y_train,epochs=15,validation_data=(x_test,y_test),batch_size=10)


# In[ ]:


loss,accuracy=model2.evaluate(x_test,y_test,verbose=False)
print("Testing accuracy: {:.4f}".format(accuracy))


# In[ ]:


#Model3 - CNN
from keras.layers import Conv1D,GlobalMaxPooling1D
embedding_dim_size=100
model3 = Sequential()
model3.add(Embedding(input_dim=vocab_size,output_dim=embedding_dim_size,input_length=maxlen)) 
model3.add(Conv1D(64,5,activation='leaky_relu'))
model3.add(GlobalMaxPooling1D())
model3.add(Dense(10,activation='leaky_relu'))
model3.add(Dense(1,activation='sigmoid'))

model3.compile(loss='binary_crossentropy',optimizer='adam',metrics=['accuracy'])
model3.summary()


# In[ ]:


history = model3.fit(x_train,y_train,epochs=15,validation_data=(x_test,y_test),batch_size=10)


# In[ ]:


loss,accuracy = model3.evaluate(x_test,y_test,verbose=False)
print("Testing accuracy: {:.4f}".format(accuracy))


# In[ ]:


#Model4 - RNN

from keras.layers import SimpleRNN,Dropout
model4 = Sequential()
model4.add(Embedding(input_dim=vocab_size,output_dim=embedding_dim_size,input_length=maxlen))
model4.add(SimpleRNN(128,return_sequences=True))
model4.add(Dropout(0.2))
model4.add(SimpleRNN(128))
model4.add(Dropout(0.2))
model4.add(Dense(10,activation='leaky_relu'))
model4.add(Dense(1,activation='sigmoid'))
model4.compile(loss='binary_crossentropy',optimizer='adam',metrics=['accuracy'])
model4.summary()


# In[ ]:


history = model4.fit(x_train,y_train,epochs=15,validation_data=(x_test,y_test),batch_size=10)


# In[ ]:


loss,accuracy = model4.evaluate(x_test,y_test,verbose=False)
print("Testing accuracy: {:.4f}".format(accuracy))


# In[ ]:


#Model5 - LSTM
from keras.layers import LSTM

embedding_dim_size = 50
model5 = Sequential()
model5.add(Embedding(input_dim=vocab_size,output_dim=embedding_dim_size,input_length=maxlen)) 
model5.add(LSTM(128,return_sequences=True))
model5.add(Dropout(0.2))
model5.add(SimpleRNN(128))
model5.add(Dense(10,activation='leaky_relu'))
model5.add(Dense(1,activation='sigmoid'))
model5.compile(loss='binary_crossentropy',optimizer='adam',metrics=['accuracy'])
model5.summary()


# In[ ]:


history = model5.fit(x_train,y_train,epochs=15,validation_data=(x_test,y_test),batch_size=10)


# In[ ]:


loss,accuracy = model5.evaluate(x_test,y_test,verbose=False)
print("Testing accuracy: {:.4f}".format(accuracy))


# ###**Glove**

# In[ ]:


get_ipython().system('pip install glove-python-binary')


# In[ ]:


import glove
from glove import Glove,Corpus
lines=[['Hello', 'this','tutorial', 'on', 'how','convert' ,'word','integer','format'],['this' ,'beautiful', 'day'],['Jack','going' , 'office']]
corpus=Corpus()
corpus.fit(lines,window=10)


# In[ ]:


glove = Glove(no_components=5, learning_rate=0.05)
glove.fit(corpus.matrix,epochs=30,no_threads=4,verbose=True)
glove.add_dictionary(corpus.dictionary)


# In[ ]:


print(glove.dictionary['Hello'])


# In[ ]:


print(glove.word_vectors[glove.dictionary['Hello']])


# In[ ]:


print(glove.most_similar('Hello'))


# In[ ]:


import gensim.downloader as api
glove_model1 = api.load('glove-twitter-25')
glove_model2 = api.load('glove-wiki-gigaword-300')
print(glove_model1['tutorial'])
print(glove_model2['tutorial'])


# ###**Word2Vec**

# In[ ]:


import gensim
from gensim.models import Word2Vec


# In[ ]:


sentences = [
    ['this','is','the','first','sentence','for','word2vec'],
    ['this','is','the','second','sentence'],
    ['yet','another','sentence'],
    ['one','more','sentence'],
    ['and','the','final','sentence']
]


# In[ ]:


w2v_model = Word2Vec(sentences,min_count=1,size=50,window=3,sg=1)
words = list(w2v_model.wv.vocab)
print(words)


# In[ ]:


print(w2v_model['this'])
w2v_model.most_similar('the')


# In[ ]:


import gensim.downloader as api
model = api.load('word2vec-google-news-300')
print(model['sentence'])

