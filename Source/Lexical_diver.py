#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import nltk
nltk.download("stopwords")
nltk.download("punkt")


# In[ ]:


text1 = '''I have bought several of the Vitality canned dog food products and have found them all to be of good quality. 
           The product looks more like a, "stew" than a processed meat and it smells better. 
           My Labrador is finicky and she appreciates this product better than  most.'''

text2 = '''Product arrived labeled as Jumbo Salted Peanuts...the peanuts were actually small sized unsalted. 
           Not sure if this was an error or if the vendor intended to represent the product as "Jumbo".'''


# In[ ]:


from nltk.corpus import stopwords

s = stopwords.words("english")



# # no of total words, distinct words, lexical diversity and all words having length greater than 5.

# In[ ]:


from nltk.tokenize import sent_tokenize,word_tokenize
import re

def word_ex(text):
  words = []
  for line in sent_tokenize(text):
    line = re.sub(r'[^\w\s]','',line)
    for word in word_tokenize(line):
      word = word.lower()
      words.append(word)
  
  return words



words1 = word_ex(text1)
words2 = word_ex(text2)

distinct_words1 = set(words1)

distinct_words2 = set(words2)

print("Total Words in both Texts: {0}".format(len(words1)+len(words2)))

print("Distinct words:\n",*distinct_words1," ",*distinct_words2)

total = (len(words1)+len(words2))/(len(set(words1))+len(set(words2)))
print("\nLexical Diersity of words in both text together: {0}".format(total))

total_words_u = set(words1+words2)
print("Distinct Words Count: {0}".format(len(total_words_u)))
print("\nWords with length greater than 5\n")
for words in total_words_u:
  if len(words)>5:
    print(words)


# # WordCloud

# In[ ]:


import wordcloud

wc = wordcloud.WordCloud()

wc.generate(text1+text2)
wc.to_image()


# # implement wor2vec model, generate the wordvectors (without pre-processing steps)

# In[ ]:


import gensim
from gensim.models import Word2Vec


# In[ ]:


def word_ex(text):
  words = []
  for line in sent_tokenize(text):
    line = re.sub(r'[^\w\s]','',line)
    w = []
    for word in word_tokenize(line):
      w.append(word)
    words.append(w)
  
  return words

t = word_ex(text1)+word_ex(text2)
print(t)


# In[ ]:


model = Word2Vec(t,min_count=1,size = 50,sg = 1, window = 5)


# In[ ]:


print(model.wv.vocab)


# In[ ]:


print(model.most_similar("peanuts"))


# In[ ]:


model.save("w2c.model")


# In[ ]:




