#!/usr/bin/env python
# coding: utf-8

# In[7]:


from tensorflow.keras.models import *
from tensorflow.keras.layers import *
from tensorflow.keras.datasets import imdb
from tensorflow.keras.preprocessing.sequence import pad_sequences
import numpy as np
import matplotlib.pyplot as plt


# In[8]:


(x_train, y_train), (x_test, y_test) = imdb.load_data()


# In[9]:


def load_vector(path, dim=50):
    vec = {}
    lines = open(path, 'r', encoding='utf-8').readlines()
    for line in lines:
        sp = line.split()
        vec[sp[0]] = np.array(sp[1:], dtype=float)
    return vec

def get_weights(vector, word_index):
    weights = np.zeros((len(word_index)+1, len(list(vector.values())[0])))
    for word, idx in word_index.items():
        if word in vector:
            weights[idx] = vector[word]
    return weights
    

glove = "/Users/vaisakh/programs/Notebooks.old/dataset/glove.6B.50d.txt"
vocab = imdb.get_word_index()

vector = load_vector(glove)
weights = get_weights(vector, vocab)


# In[10]:


max_len = 100

x_train = pad_sequences(x_train, max_len)
x_test = pad_sequences(x_test, max_len)


# In[11]:


model = Sequential()
model.add(Embedding(input_dim=len(vocab)+1, output_dim=50, input_length=max_len, weights=[weights], trainable=True))
model.add(LSTM(units=20, return_sequences=True))
model.add(Dropout(.2))
model.add(LSTM(units=10, return_sequences=False))
model.add(Dense(256, activation='relu'))
model.add(Dropout(.2))
model.add(Dense(128, activation='relu'))
model.add(Dropout(.1))
model.add(Dense(64, activation='relu'))
model.add(Dense(32, activation='relu'))
model.add(Dense(1, activation='sigmoid'))


# In[12]:


model.summary()


# In[13]:


model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])


# In[14]:


history = model.fit(x_train, y_train, batch_size=100, epochs=5, validation_split=.2).history


# In[15]:


err, acc = model.evaluate(x_test, y_test)
acc


# In[16]:


plt.style.use("ggplot")

plt.subplot(2,1,1)
plt.title("crossentropy loss")
plt.plot(history['loss'], marker='o')
plt.plot(history['val_loss'], marker='o')
plt.tight_layout(pad=2)

plt.subplot(2,1,2)
plt.title("crossentropy accuracy")
plt.plot(history['accuracy'], marker='o')
plt.plot(history['val_accuracy'], marker='o')
plt.tight_layout(pad=2)


# In[ ]:




